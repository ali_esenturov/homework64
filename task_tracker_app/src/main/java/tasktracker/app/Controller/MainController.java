package tasktracker.app.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import tasktracker.app.Model.Task;
import tasktracker.app.Service.TaskService;


@Controller
public class MainController {
    private final TaskService ts;

    public MainController(TaskService ts) {
        this.ts = ts;
    }

    @GetMapping("/")
    public String demo(Model model){
        return "index";
    }

    @PostMapping("/addTask")
    public String addPost(@RequestParam("name") String name, @RequestParam("description") String description){
        Task task = new Task(name, description);
        ts.addTask(task);
        return "index";
    }

    @PostMapping("/changeStatus/{id}")
    public String changeStatus(@PathVariable("id") String id){
        Task task = ts.getTaskById(id);
        task.changeStatus();
        ts.addTask(task);
        return "index";
    }

    @PostMapping("/deleteTask/{id}")
    public String deleteTask(@PathVariable("id") String id){
        ts.removeTaskById(id);
        return "index";
    }
}