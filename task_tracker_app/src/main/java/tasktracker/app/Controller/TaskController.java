package tasktracker.app.Controller;

import tasktracker.app.Model.Task;
import tasktracker.app.Service.TaskService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tasks")
public class TaskController {
    final private TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/get_all")
    public List<Task> getAllTasks(){
        return taskService.getAllTasks();
    }
}