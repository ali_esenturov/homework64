package tasktracker.app.Model;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TaskRepository extends CrudRepository<Task, String> {
    List<Task> findAll();
    Task findTaskById(String id);
}
