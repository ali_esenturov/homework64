package tasktracker.app.Service;

import org.springframework.stereotype.Service;
import tasktracker.app.Model.Task;
import tasktracker.app.Model.TaskRepository;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {
    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> getAllTasks(){
        return taskRepository.findAll();
    }

    public void addTask(Task task){
        taskRepository.save(task);
    }

    public Task getTaskById(String id){
        Optional<Task> taskOpt = taskRepository.findById(id);
        return taskOpt.orElse(null);
    }

    public void removeTaskById(String id){
        taskRepository.deleteById(id);
    }
}
