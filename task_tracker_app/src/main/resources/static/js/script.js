'use strict';
const baseUrl = 'http://localhost:9889';

window.addEventListener('load', function () {
    const taskForm = document.getElementById("task-form");
    taskForm.addEventListener('submit', addTaskHandler);

    function addTaskHandler(e){
        e.preventDefault();
        let form = e.target;
        let data = new FormData(form);

        fetch('http://localhost:9889/addTask', {
            method: 'POST',
            body: data
        });

        taskForm.reset();
        document.getElementById("name").focus();
        window.location.href = baseUrl;
    }
});

function createTaskElement(task){
    let content = '<p id="' + task.id + '"' + ' ondblclick="changeStatusOfTask(this.id);"' + '>' +
        task.name + ' : '+ task.description + '</p>' +
        '<button id="change' + task.id + '"' + ' type="button" class="changeStatusButton" onclick="changeStatusButton(this.id)"> CHANGE STATUS </button>' +
        '<button id="delete' + task.id + '"' + ' type="button" class="deleteTaskButton" onclick="deleteTaskButton(this.id)"> DELETE </button>';

    let taskElement = document.createElement('li');
    taskElement.innerHTML += content;
    return taskElement;
}

function changeStatusOfTask(id){
    fetch('http://localhost:9889/changeStatus/' + id, {
        method: 'POST'
    });
    let task = document.getElementById(id);
    if(task.classList.contains("taskView")){
        task.classList.remove("taskView");
    }
    else{
        task.classList.add("taskView");
    }
}

function changeStatusButton(id){
    let task_id = id.replace('change', '');
    changeStatusOfTask(task_id);
}

function deleteTaskButton(id){
    let task_id = id.replace('delete', '');
    fetch('http://localhost:9889/deleteTask/' + task_id, {
        method: 'POST'
    });
    document.getElementById(id).parentElement.remove();
}

async function getTasks(){
    let url = 'http://localhost:9889/tasks/get_all';
    let response = await fetch(url);
    return await response.json();
}

async function addingTasks(){
    let tasks = await getTasks();
    for(let i=0; i<tasks.length; i++){
        let task = {
            id: tasks[i].id,
            name: tasks[i].name,
            description: tasks[i].description,
            done: tasks[i].done
        };
        let taskElement = createTaskElement(task);
        if(task.done === true){
            taskElement.children[0].classList.add("taskView");
        }
        document.getElementById("task-list").appendChild(taskElement);
    }
}